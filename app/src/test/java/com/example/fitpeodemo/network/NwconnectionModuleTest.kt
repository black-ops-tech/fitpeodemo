package com.example.fitpeodemo.network

import com.example.fitpeodemo.utils.Constants.BASE_URL
import org.junit.Assert.*

import org.junit.Test
import retrofit2.Retrofit

class NwconnectionModuleTest {

    @Test
    fun provideRetrofit() {
        val instance: Retrofit = NwconnectionModule.provideRetrofit(
            NwconnectionModule.provideHttpClient(),
            NwconnectionModule.provideConverterFactory()
        )
        assert(instance.baseUrl().url().toString() == BASE_URL)
    }
}