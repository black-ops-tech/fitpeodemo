package com.example.fitpeodemo.network

import com.example.fitpeodemo.model.DemoModelItem
import com.example.fitpeodemo.utils.Constants
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface DemoService {
    @GET(Constants.BASE_URL)
    fun getData(): Call<List<DemoModelItem>>
}