package com.example.fitpeodemo.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.fitpeodemo.databinding.ActivityDetailBinding
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {

    lateinit var bind: ActivityDetailBinding
    var id: Int = 0
    lateinit var title: String
    var albumId: Int = 0
    lateinit var url: String
    lateinit var thumbnailUrl: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(bind.root)
        getDataForDetail()
        setViewContents()
    }

    private fun setViewContents() {
        bind.albumId.text = albumId.toString()
        bind.textId.text = id.toString()
        bind.title.text = title
        Picasso.get().load(url).into(bind.urlImageView)
    }

    private fun getDataForDetail() {
        id = intent.getIntExtra("ID", 0)
        title = intent.getStringExtra("TITLE").toString()
        albumId = intent.getIntExtra("ALBUMID", 0)
        url = intent.getStringExtra("URL").toString()
        thumbnailUrl = intent.getStringExtra("THUMBNAILURL").toString()
    }
}