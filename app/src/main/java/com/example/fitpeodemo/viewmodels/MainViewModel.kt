package com.example.fitpeodemo.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.fitpeodemo.model.DemoModelItem
import com.example.fitpeodemo.repositories.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: Repository, application: Application
) : AndroidViewModel(application) {

    private val _response: MutableLiveData<List<DemoModelItem>> = MutableLiveData()
    val res: LiveData<List<DemoModelItem>> = _response

    fun fetchDemoDataResponse() {
        repository.getDemoData().enqueue(object : Callback<List<DemoModelItem>> {
            override fun onResponse(
                call: Call<List<DemoModelItem>>,
                response: Response<List<DemoModelItem>>
            ) {
                _response.value = response.body()
            }

            override fun onFailure(call: Call<List<DemoModelItem>>, t: Throwable) {
                Log.d("TAG", t.message.toString())
            }
        })
    }
}