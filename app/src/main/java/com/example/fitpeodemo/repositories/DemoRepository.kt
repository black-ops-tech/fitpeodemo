package com.example.fitpeodemo.repositories

import com.example.fitpeodemo.network.DemoService
import javax.inject.Inject


class DemoRepository @Inject constructor(private val demoService: DemoService) {
    fun getDemoData() = demoService.getData()
}