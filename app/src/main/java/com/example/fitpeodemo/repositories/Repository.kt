package com.example.fitpeodemo.repositories

import com.example.fitpeodemo.network.BaseApiResponse
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

@ActivityRetainedScoped
class Repository @Inject constructor(private val demoRepository: DemoRepository) :
    BaseApiResponse() {
    fun getDemoData() = demoRepository.getDemoData()
}

