package com.example.fitpeodemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fitpeodemo.adapters.MainDemoListAdapter
import com.example.fitpeodemo.databinding.ActivityMainBinding
import com.example.fitpeodemo.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val mainViewModel by viewModels<MainViewModel>()
    private lateinit var _binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        fetchData()
    }

    private fun fetchData() {
        mainViewModel.fetchDemoDataResponse()
        mainViewModel.res.observe(this) { response ->
            _binding.appRecyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = MainDemoListAdapter(context, response)
            }
        }
    }
}