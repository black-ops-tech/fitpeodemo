package com.example.fitpeodemo.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.fitpeodemo.R
import com.example.fitpeodemo.adapters.MainDemoListAdapter.MyViewHolder
import com.example.fitpeodemo.model.DemoModelItem
import com.example.fitpeodemo.ui.DetailActivity
import kotlinx.android.synthetic.main.mainitem_layout.view.*

class MainDemoListAdapter(context: Context, list: List<DemoModelItem>) :
    RecyclerView.Adapter<MyViewHolder>() {

    private val listAp = list
    private val ctx = context

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(ctx: Context, listAp: List<DemoModelItem>, position: Int) {
            itemView.textView_list_title.text = listAp.get(position).title

            itemView.card_on_item_layout.setOnClickListener(View.OnClickListener {
                val intent = Intent(ctx, DetailActivity::class.java)
                intent.putExtra("ID", listAp.get(position).id)
                intent.putExtra("TITLE", listAp.get(position).title)
                intent.putExtra("ALBUMID", listAp.get(position).albumId)
                intent.putExtra("URL", listAp.get(position).url)
                intent.putExtra("THUMBNAILURL", listAp.get(position).thumbnailUrl)
                ctx.startActivity(intent)
            })
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder = MyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.mainitem_layout, parent, false)
    )


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(ctx, listAp, position)
    }

    override fun getItemCount() = listAp.size
}